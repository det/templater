modules.define('Templater', {autoDefine: false}, function () {
    var getResult = function (result, data) {
        if (typeof result === 'function') {
            return result();
        } else if (result !== null && result !== undefined && result.indexOf('{{') !== -1){
            var value = data[result.match(/{{(.*?)}}/)[1]];
            if (Array.isArray(value)){
                return value.reduce(function (res, newValue) {
                    return res + result.replace(/{{(.*?)}}/, newValue);
                }, '');
            } else {
                return templater(result, data);
            }
        } else {
            return result;
        }
    };

    var templater = function (html, data) {
        return html.replace(/{{(.*?)}}/g, function (matched, variable) {
            if (data.hasOwnProperty(variable)) {
                return getResult(data[variable], data);
            } else if (variable.indexOf('.') !== -1){
                var variableArray = variable.split('.');
                var result = data;
                variableArray.forEach(function (vari) {
                    result = result[vari];
                });
                return getResult(result, data);
            } else {
                return ''
            }
        })
    };

    return {run: templater};
});