#ATemplater

CD API module with soma varints of changed content.

Using

```javascript
var data = {
    simpleValue: 'test1',
    values: {
        firstValue: 'test2',
        secondValue: {
            secondValueB: 'test3'
            }
        },
    valuesFunc: function(){
        return 'functionValue'
        },
    subValue: 'test Sub Value Wrapper {{simpleValue}}',
    arrayData: ['s', 'o', 'm', 'e'],
    arrayWrapper: '<li>{{arrayData}}</li>',
    arrayData1: [],
    arrayWrapper1: '<h3>{{arrayData1}}</h3>'
};
```
Values could be described as:
simple object property,
multilevel object property,
function result,
html text with another data property,
array

```javascript
var html = '' +
'Some text simpleValue = {{simpleValue}},\n' +
'Some text values.firstValue{{values.firstValue}},\n' +
'Some text values.secondValue.secondValueB = {{values.secondValue.secondValueB}},\n' +
'Some text valuesFunc = {{valuesFunc}},\n' +
'Some text subValue = {{subValue}}\n' +
'Some text arrayWrapper = <ul>{{arrayWrapper}}</ul>\n' +
'Some text arrayWrapper1 = {{arrayWrapper1}}';

var templater = modules.require('Templater');
console.log(templater.run(html, data));
```
result:
```javascript
Some text simpleValue = test1,
Some text values.firstValuetest2,
Some text values.secondValue.secondValueB = test3,
Some text valuesFunc = functionValue,
Some text subValue = test Sub Value Wrapper test1
Some text arrayWrapper = <ul><li>s</li><li>o</li><li>m</li><li>e</li></ul>
Some text arrayWrapper1 =
```